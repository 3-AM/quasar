$(document).ready(function(){
    var brand_choice = $(".inquiry_wrap .brand_choice").find('label.radio');
    brand_choice.click(function() {
        brand_choice.removeClass("radio_checked").not(brand_choice).add(this).toggleClass("radio_checked");
      });
    var steering_wheel = $(".inquiry_wrap .steering_wheel").find('label.radio');
    steering_wheel.click(function() {
        steering_wheel.removeClass("radio_checked").not(steering_wheel).add(this).toggleClass("radio_checked");
      });
    $(".inquiry_wrap").find('label.checkbox').on('change', function() {
      $(this).toggleClass("checkbox_checked");
    });
     $('.inquiry_wrap').find('.hidden').hide();
      $('.inquiry_wrap .open_hide').click(function(e){
        $(this).toggleClass('active').next().fadeToggle();
        e.preventDefault();
      })
});